package az.eyvazov.factory;

public class Mercedes implements Car{

    @Override
    public int horsePower() {
        return 170;
    }

    @Override
    public int maxSpeed() {
        return 350;
    }
}
