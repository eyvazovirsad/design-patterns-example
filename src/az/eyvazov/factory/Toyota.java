package az.eyvazov.factory;

public class Toyota implements Car {

    @Override
    public int horsePower() {
        return 150;
    }

    @Override
    public int maxSpeed() {
        return 300;
    }
}
