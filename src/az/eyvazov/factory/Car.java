package az.eyvazov.factory;

public interface Car {
    public int horsePower();

    public int maxSpeed();
}
