package az.eyvazov.factory;

public class CarFactory {

    public Car getCar(String car){
        if(car.equalsIgnoreCase("BMW")){
            return new Mercedes();
        }else if(car.equalsIgnoreCase("MERCEDES")){
            return new Mercedes();
        }else if(car.equalsIgnoreCase("TOYOTA")){
            return new Toyota();
        }
        return null;
    }
}
