package az.eyvazov.factory;

public  class BMW implements Car {

    @Override
    public int horsePower() {
        return 200;
    }

    @Override
    public int maxSpeed() {
        return 400;
    }
}
