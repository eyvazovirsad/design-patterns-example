package az.eyvazov.singleton;

public class DbConnection {

    public static DbConnection INSTANCE;


    private DbConnection() {
    }

    public static DbConnection getInstance() {
        if (INSTANCE == null) {
            synchronized (DbConnection.class) {
                if (INSTANCE == null) {
                    INSTANCE = new DbConnection();
                }
            }
        }
        return INSTANCE;
    }
}
