package az.eyvazov.abstractfactory;

public interface Deposit {
    public String name();
}
