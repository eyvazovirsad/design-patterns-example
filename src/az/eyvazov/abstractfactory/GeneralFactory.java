package az.eyvazov.abstractfactory;

public class GeneralFactory {

    public static Factory getFactory(String name){
        if(name.equalsIgnoreCase("deposit")){
            return new DepositFactory();
        }else{
            return new AccountFactory();
        }
    }
}
