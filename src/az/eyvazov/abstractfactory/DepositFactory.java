package az.eyvazov.abstractfactory;

public class DepositFactory implements Factory {

    @Override
    public Account getAccount(String name) {
        return null;
    }

    @Override
    public Deposit getDeposit(String name) {
        if(name.equalsIgnoreCase("express")){
            return new ExpressDeposit();
        }else{
            return new UsefulDeposit();
        }
    }
}
