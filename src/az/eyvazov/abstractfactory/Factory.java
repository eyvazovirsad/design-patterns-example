package az.eyvazov.abstractfactory;

public interface Factory {
    public Account getAccount(String name);
    public Deposit getDeposit(String name);
}
