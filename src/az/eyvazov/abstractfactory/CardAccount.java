package az.eyvazov.abstractfactory;

public class CardAccount implements Account {
    @Override
    public int maxLimit() {
        return 10000;
    }
}
