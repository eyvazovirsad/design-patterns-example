package az.eyvazov.abstractfactory;

public class AccountFactory implements Factory {

    @Override
    public Account getAccount(String name){
        if(name.equalsIgnoreCase("current")){
            return new CurrentAccount();
        }else{
            return new CardAccount();
        }
    }

    @Override
    public Deposit getDeposit(String name) {
        return null;
    }
}
