package az.eyvazov.abstractfactory;

public class CurrentAccount implements Account {
    @Override
    public int maxLimit() {
        return 20000;
    }
}
