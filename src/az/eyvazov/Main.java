package az.eyvazov;

import az.eyvazov.abstractfactory.*;
import az.eyvazov.factory.Car;
import az.eyvazov.factory.CarFactory;

public class Main {

    public static void main(String[] args) {

        //Factory
        CarFactory carFactory = new CarFactory();
        Car car = carFactory.getCar("BMW");
        System.out.println("Max Speed: " + car.maxSpeed());
        System.out.println("Horse speed: " + car.horsePower());


        //Abstract Factory
        Factory factory = GeneralFactory.getFactory("deposit");
        Account account = factory.getAccount("current");
        System.out.println("Account max limit: " + account.maxLimit());
        Factory factory2 = GeneralFactory.getFactory("account");
        Deposit deposit = factory2.getDeposit("express");
        System.out.println("Deposit name: " + deposit.name());

    }
}
